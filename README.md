Elite Schedule Mock Application
======
POC application for Ionic and Electron demonstration

## How to use

1. Install yarn
2. Run `yarn install`
3. `ionic build`
4. `yarn dist`

## Building
This POC app was created using the following guides:

1. [Ionic Getting Started](https://ionicframework.com/getting-started#cli)
2. [Electron Builder](https://github.com/electron-userland/electron-builder)