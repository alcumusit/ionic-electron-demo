import { EliteApiProvider } from './../../providers/elite-api/elite-api';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import * as _ from 'lodash';

@Component({
  selector: 'page-team-detail',
  templateUrl: 'team-detail.html',
})
export class TeamDetailPage {

  public team: any = {};
  public games: any[];
  private tournamentData: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private eliteApiProvider: EliteApiProvider) {
    
  }

  ionViewDidLoad() {
    this.team = this.navParams.data;

    this.tournamentData = this.eliteApiProvider.getCurrentTournament();

    this.games = _.chain(this.tournamentData.games)
                .filter(g => g.team1Id === this.team.id || g.team2Id === this.team.id)
                .map(g => {
                  let isTeam1 = (g.team1Id === this.team.id);
                  let opponentName = (g.team1Id ? g.team2 : g.team1);
                  let scoreDisplay = this.getScoreDisplay(isTeam1, g.team1Score, g.team2Score);
                  return {
                    gameId: g.id,
                    opponent: opponentName,
                    time: Date.parse(g.time),
                    location: g.location,
                    locationUrl: g.locationUrl,
                    scoreDisplay,
                    homeAway: (isTeam1 ? 'vs' : 'at')
                  }
                })
                .value();
  }

  getScoreDisplay(isTeam1, team1Score, team2Score) {
    if (team1Score && team2Score) {
      const teamScore = (isTeam1 ? team1Score : team2Score);
      const opponentScore = (isTeam1 ? team2Score : team1Score);
      const winIndicator = teamScore > opponentScore ? 'W: ' : 'L: ';
      return `${winIndicator}${teamScore}-${opponentScore}`;
    }
    
    return '';
  }
}
