var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { EliteApiProvider } from './../../providers/elite-api/elite-api';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import * as _ from 'lodash';
var TeamDetailPage = /** @class */ (function () {
    function TeamDetailPage(navCtrl, navParams, eliteApiProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.eliteApiProvider = eliteApiProvider;
        this.team = {};
    }
    TeamDetailPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.team = this.navParams.data;
        this.tournamentData = this.eliteApiProvider.getCurrentTournament();
        this.games = _.chain(this.tournamentData.games)
            .filter(function (g) { return g.team1Id === _this.team.id || g.team2Id === _this.team.id; })
            .map(function (g) {
            var isTeam1 = (g.team1Id === _this.team.id);
            var opponentName = (g.team1Id ? g.team2 : g.team1);
            var scoreDisplay = _this.getScoreDisplay(isTeam1, g.team1Score, g.team2Score);
            return {
                gameId: g.id,
                opponent: opponentName,
                time: Date.parse(g.time),
                location: g.location,
                locationUrl: g.locationUrl,
                scoreDisplay: scoreDisplay,
                homeAway: (isTeam1 ? 'vs' : 'at')
            };
        })
            .value();
    };
    TeamDetailPage.prototype.getScoreDisplay = function (isTeam1, team1Score, team2Score) {
        if (team1Score && team2Score) {
            var teamScore = (isTeam1 ? team1Score : team2Score);
            var opponentScore = (isTeam1 ? team2Score : team1Score);
            var winIndicator = teamScore > opponentScore ? 'W: ' : 'L: ';
            return "" + winIndicator + teamScore + "-" + opponentScore;
        }
        return '';
    };
    TeamDetailPage = __decorate([
        Component({
            selector: 'page-team-detail',
            templateUrl: 'team-detail.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            EliteApiProvider])
    ], TeamDetailPage);
    return TeamDetailPage;
}());
export { TeamDetailPage };
//# sourceMappingURL=team-detail.js.map