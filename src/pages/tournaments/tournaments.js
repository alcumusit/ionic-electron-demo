var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { EliteApiProvider } from './../../providers/elite-api/elite-api';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { TeamsPage } from '../teams/teams';
var TournamentsPage = /** @class */ (function () {
    function TournamentsPage(navCtrl, navParams, eliteApiProvider, loadingController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.eliteApiProvider = eliteApiProvider;
        this.loadingController = loadingController;
    }
    TournamentsPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        var loader = this.loadingController.create({
            content: 'Getting tournaments.....'
        });
        loader.present().then(function () {
            _this.eliteApiProvider.getTournaments().then(function (data) {
                _this.tournaments = data;
                loader.dismiss();
            });
        });
    };
    TournamentsPage.prototype.itemTapped = function ($event, tournament) {
        this.navCtrl.push(TeamsPage, tournament);
    };
    TournamentsPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-tournaments',
            templateUrl: 'tournaments.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            EliteApiProvider,
            LoadingController])
    ], TournamentsPage);
    return TournamentsPage;
}());
export { TournamentsPage };
//# sourceMappingURL=tournaments.js.map