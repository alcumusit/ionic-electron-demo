var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
var EliteApiProvider = /** @class */ (function () {
    function EliteApiProvider(http) {
        this.http = http;
        this.baseUrl = 'http://localhost:3000/elite-teams';
        this.currentTourmanent = {};
    }
    EliteApiProvider.prototype.getTournaments = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http.get(_this.baseUrl + "/tournaments").subscribe(function (res) { return resolve(res.json()); });
        });
    };
    EliteApiProvider.prototype.getTournamentData = function (tournamentId) {
        var _this = this;
        return this.http.get(this.baseUrl + "/tournaments-data/" + tournamentId)
            .map(function (response) {
            _this.currentTourmanent = response.json();
            return _this.currentTourmanent;
        });
    };
    EliteApiProvider.prototype.getCurrentTournament = function () {
        return this.currentTourmanent;
    };
    EliteApiProvider = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [Http])
    ], EliteApiProvider);
    return EliteApiProvider;
}());
export { EliteApiProvider };
//# sourceMappingURL=elite-api.js.map