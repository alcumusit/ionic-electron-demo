import { Http } from '@angular/http'
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class EliteApiProvider {
  private baseUrl = 'http://localhost:3000/elite-teams';
  private currentTourmanent: any = {};

  constructor(public http: Http) {
    
  }

  getTournaments() {
    return new Promise(resolve => {
      this.http.get(`${this.baseUrl}/tournaments`).subscribe(res => resolve(res.json()));
    }); 
  }

  getTournamentData(tournamentId): Observable<any> {
    return this.http.get(`${this.baseUrl}/tournaments-data/${tournamentId}`)
      .map(response => {
        this.currentTourmanent = response.json();
        return this.currentTourmanent;
      })
  }

  getCurrentTournament() {
    return this.currentTourmanent;
  }
}
